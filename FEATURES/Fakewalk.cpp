#include "../includes.h"
#include "../UTILS/interfaces.h"
#include "../SDK/IEngine.h"
#include "../SDK/CUserCmd.h"
#include "../SDK/CBaseEntity.h"
#include "../SDK/CClientEntityList.h"
#include "../SDK/CTrace.h"
#include "../SDK/CBaseWeapon.h"
#include "../SDK/CGlobalVars.h"
#include "../SDK/NetChannel.h"
#include "../SDK/CBaseAnimState.h"
#include "../SDK/ConVar.h"
#include "../FEATURES/AutoWall.h"
#include "../FEATURES/Fakewalk.h"
#include "../FEATURES/Aimbot.h"
#include "AntiAim.h"
#include <time.h>
#include <iostream>

void CFakewalk::do_fakewalk(SDK::CUserCmd* cmd) {

	if (!Settings::Options.fakewalk) {

		fake_walk = false;
		GLOBAL::is_fakewalking = false;
		return;
	}

	if (GetAsyncKeyState(VK_SHIFT)) {

		static int choked = 0;
		auto local_player = g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer());
		if (!local_player || local_player->GetHealth() <= 0) return;

		//auto net_channel = I::Engine->GetNetChannel();
		//if (!net_channel) return;

		auto animstate = local_player->GetAnimState();
		if (!animstate) return;

		fake_walk = true;
		GLOBAL::is_fakewalking = true;

		if (fabs(local_update - g_csgo::Globals->curtime) <= 0.1)
		{
			GLOBAL::originalCMD.move.x = 450;
			aimbot->rotate_movement(UTILS::CalcAngle(Vector(0, 0, 0), local_player->GetVelocity()).y + 180.f, cmd);
		}

		choked = (choked > Settings::Options.fakewalkspeed) ? 0 : choked + 1;
		GLOBAL::originalCMD.move.x = choked < 2 || choked > 5 ? 0 : GLOBAL::originalCMD.move.x;
		GLOBAL::originalCMD.move.y = choked < 2 || choked > 5 ? 0 : GLOBAL::originalCMD.move.y;
		GLOBAL::should_send_packet = choked < 1;
	}
	else {
		fake_walk = false;
		GLOBAL::is_fakewalking = false;
	}
}

/*
void CFakewalk::do_fakewalk(SDK::CUserCmd* cmd) {

	GLOBAL::is_fakewalking = false;

	if (!Settings::Options.fakewalk)
		return;

	if (GetAsyncKeyState(VK_SHIFT)) {

		auto local_player = g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer());
		if (!local_player || local_player->GetHealth() <= 0) return;

		auto net_channel = g_csgo::Engine->GetNetChannel();
		if (!net_channel) return;

		auto animstate = local_player->GetAnimState();
		if (!animstate) return;

		GLOBAL::is_fakewalking = true;

		int32_t ticks_to_update = (antiaim->m_next_lby_update_time - 1);

		const int32_t max_ticks = std::min<int32_t>(7, ticks_to_update);

		const int32_t chocked = net_channel->m_nChokedPackets;

		int32_t ticks_left = max_ticks - chocked;

		if (!net_channel->m_nChokedPackets) {

			if (antiaim->TimeUntilNextLBYUpdate() > 0.15)
				GLOBAL::should_choke_packets = true;
			else
				GLOBAL::should_choke_packets = false;
		}

		GLOBAL::originalCMD.move.y *= Settings::Options.fakewalkspeed / 233.33;
		GLOBAL::originalCMD.move.x *= Settings::Options.fakewalkspeed / 233.33;

		if (GLOBAL::should_choke_packets) {

			const int choked_ticks = net_channel->m_nChokedPackets;

			if (!choked_ticks || ticks_left > (animstate->speed_2d < 115.f ? 9 : 8)) {

				GLOBAL::originalCMD.move.y = 0;
				GLOBAL::originalCMD.move.x = 450;

				if (!choked_ticks || animstate->speed_2d < 20.f)
					GLOBAL::originalCMD.move.x = 0;

				aimbot->rotate_movement(UTILS::CalcAngle(Vector(0, 0, 0), local_player->GetVelocity()).y + 180.f, cmd);
			}
		} else {
			GLOBAL::originalCMD.move.y = 0;
			GLOBAL::originalCMD.move.x = 0;
		}
	}
}
*/
CFakewalk* slidebitch = new CFakewalk();