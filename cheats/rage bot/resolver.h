#pragma once
/*
#include "..\..\singleton.h"
#include "..\..\SDK\IEngine.h"
#include "..\..\SDK\CBaseEntity.h"
#include "..\..\SDK\CUserCmd.h"
#include "..\..\SDK\CGlobalVars.h"
#include "..\..\UTILS\render.h"

struct resolver_info {

	resolver_info() {

		m_bActive = false;

		m_flVelocity = 0.f;
		m_vecVelocity = Vector(0, 0, 0);
		m_angEyeAngles = QAngle(0, 0, 0);
		m_flSimulationTime = 0.f;
		m_flLowerBodyYawTarget = 0.f;

		m_flStandingTime = 0.f;
		m_flMovingLBY = 0.f;
		m_flLbyDelta = 180.f;
		m_bIsMoving = false;

		m_angDirectionFirstMoving = QAngle(0, 0, 0);
		m_nCorrectedFakewalkIdx = 0;
	}

	void SaveRecord(SDK::CBaseEntity* player) {

		m_flLowerBodyYawTarget = player->GetLowerBodyYaw();
		m_flSimulationTime = player->GetSimTime();
		m_flVelocity = player->GetVelocity().Length2D();
		m_vecVelocity = player->GetVelocity();
		m_angEyeAngles = *player->EasyEyeAngles();

		m_iLayerCount = *(int32_t*)player->GetAnimOverlays();

		for (int i = 0; i < m_iLayerCount; i++)
			animationLayer[i] = player->GetAnimOverlays()[i];
	}

	bool operator == (const resolver_info& other) { return other.m_flSimulationTime == m_flSimulationTime; };

	bool 
		m_bActive, 
		m_bIsMoving;

	Vector m_vecVelocity;

	float_t
		m_flVelocity,
		m_flSimulationTime,
		m_flLowerBodyYawTarget,
		m_flStandingTime,
		m_flMovingLBY,
		m_flLbyDelta;

	int32_t
		m_iLayerCount = 0,
		m_nCorrectedFakewalkIdx,
		m_nShotsMissed = 0;

	SDK::CAnimationLayer animationLayer[15];

	QAngle 
		m_angDirectionFirstMoving,
		m_angEyeAngles;
};

class cplayer_resolver {//: singleton<cplayer_resolver> {

public:

private:
	void store_values(SDK::CBaseEntity* entity, SDK::CUserCmd* cmd);
	void resolve(SDK::CBaseEntity* entity, SDK::CUserCmd* cmd);
	void override();

	float_t lby_backtrack(SDK::CBaseEntity* entity, SDK::CUserCmd* cmd);
	float_t resolve_fakewalk(SDK::CBaseEntity * entity, resolver_info & curTicRecord);

	bool is_fake_walking(SDK::CBaseEntity * entity, resolver_info & record);
	bool is_valid(int entity);

	resolver_info arr_infos[65];
};*/