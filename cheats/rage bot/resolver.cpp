#include "resolver.h"
/*
void cplayer_resolver::store_values(SDK::CBaseEntity* entity, SDK::CUserCmd* cmd) {

	auto local_player = static_cast<SDK::CBaseEntity*>(g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer()));

	int shots_fired = GLOBAL::missed_shots[local_player->GetIndex()] % 5;

	auto eye_angles = entity->GetEyeAngles();

	Vector aim_angle = MATH::NormalizeAngle(local_player->GetEyePosition());

	switch (shots_fired) {

		case 0: eye_angles.y = aim_angle.y -  0; break;
		case 1: eye_angles.y = aim_angle.y - 15; break;
		case 2: eye_angles.y = aim_angle.y + 40; break;
		case 3: eye_angles.y = aim_angle.y - 40; break;
		case 4: eye_angles.y = aim_angle.y + 15; break;
	}
}

float_t cplayer_resolver::resolve_fakewalk(SDK::CBaseEntity* entity, resolver_info& cur_tick_record) {

	auto& record = arr_infos[entity->GetIndex()];

	float_t yaw;
	int32_t correctedFakewalkIdx = record.m_nCorrectedFakewalkIdx;

	if (correctedFakewalkIdx < 2)
		yaw = record.m_angDirectionFirstMoving.yaw + 180.f;

	else if (correctedFakewalkIdx < 4)
		yaw = entity->GetLowerBodyYaw() + record.m_flLbyDelta;

	else if (correctedFakewalkIdx < 6)
		yaw = record.m_angDirectionFirstMoving.yaw;

	else {

		Vector dir;
		MATH::VectorAngles2(cur_tick_record.m_vecVelocity, dir);

		yaw = dir.y;
	} return yaw;
}

float_t cplayer_resolver::lby_backtrack(SDK::CBaseEntity* entity, SDK::CUserCmd* cmd) {

	
}

bool cplayer_resolver::is_fake_walking(SDK::CBaseEntity* entity, resolver_info& record) {

	bool
		bFakewalking = false,
		stage1 = false,			// stages needed cause we are iterating all layers, eitherwise won't work :)
		stage2 = false,
		stage3 = false;

	for (int i = 0; i < record.m_iLayerCount; i++)
	{
		if (record.animationLayer[i].m_nSequence == 26 && record.animationLayer[i].m_flWeight < 0.4f)
			stage1 = true;
		if (record.animationLayer[i].m_nSequence == 7 && record.animationLayer[i].m_flWeight > 0.001f)
			stage2 = true;
		if (record.animationLayer[i].m_nSequence == 2 && record.animationLayer[i].m_flWeight == 0)
			stage3 = true;
	}

	if (stage1 && stage2)
		if (stage3 || (entity->GetFlags() & FL_DUCKING)) // since weight from stage3 can be 0 aswell when crouching, we need this kind of check, cause you can fakewalk while crouching, thats why it's nested under stage1 and stage2
			bFakewalking = true;
		else
			bFakewalking = false;
	else
		bFakewalking = false;

	return bFakewalking;
}

bool cplayer_resolver::is_valid(int entity) {

	auto local_player = g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer());
	auto pEntity = g_csgo::ClientEntityList->GetClientEntity(entity);

	if (!pEntity)
		return false;

	if (pEntity == local_player) 
		return false;

	if (pEntity->GetTeam() == local_player->GetTeam()) 
		return false;

	if (!pEntity->GetIsDormant())
		return false;

	if (!pEntity->GetImmunity()) 
		return false;

	if (!pEntity->IsAlive()) 
		return false;
	return true;
}

void cplayer_resolver::override() {

	if (!Settings::Options.overrideenable)
		return;

	if (!GetAsyncKeyState(Settings::Options.overridekey))
		return;

	int w, h, cx, cy;

	g_csgo::Engine->GetScreenSize(w, h);

	cx = w / 2;
	cy = h / 2;

	Vector crosshair = Vector(cx, cy, 0);

	auto nearest_player = static_cast<SDK::CBaseEntity*>(nullptr);
	float best_fov = 0;
	Vector best_head_2d;

	for (int i = 1; i <= g_csgo::Globals->maxclients; i++) {

		SDK::CBaseEntity* entity = (SDK::CBaseEntity*)g_csgo::ClientEntityList->GetClientEntity(i);

		if (!is_valid(*(int*)entity))
			continue;

		Vector headPos3D = entity->GetBonePosition(SDK::HITBOX_HEAD), head_pos_2d;

		if (!RENDER::WorldToScreen(headPos3D, head_pos_2d))
			continue;

		float fov_2d = crosshair.DistTo(head_pos_2d);

		if (!nearest_player || fov_2d < best_fov) {

			nearest_player = entity;
			best_fov = fov_2d;
			best_head_2d = head_pos_2d;
		}
	}

	if (nearest_player) {

		int 
			minX = cx - (w / 10), maxX = cx + (w / 10), 
			totalWidth = maxX - minX,
			playerX = best_head_2d.x - minX,
			yawCorrection = -(((playerX * 360) / totalWidth) - 180);

		if (best_head_2d.x < minX || best_head_2d.x > maxX)
			return;

		float_t new_yaw = yawCorrection;

		MATH::clamp_yaw(new_yaw);

		nearest_player->EasyEyeAngles()->yaw += new_yaw;
	}
}

void cplayer_resolver::resolve(SDK::CBaseEntity* entity, SDK::CUserCmd* cmd) {

	auto eye_angles = entity->GetEyeAngles();

	if (eye_angles.x < -179.f)
		eye_angles.x += 360.f;

	else if (eye_angles.x > 90.0 || eye_angles.x < -90.0)
		eye_angles.x = 89.f;

	else if (eye_angles.x > 89.0 && eye_angles.x < 91.0)
		eye_angles.x -= 90.f;

	else if (eye_angles.x > 179.0 && eye_angles.x < 181.0)
		eye_angles.x -= 180;

	else if (eye_angles.x > -179.0 && eye_angles.x < -181.0)
		eye_angles.x += 180;

	else if (!fabs(eye_angles.x))
		eye_angles.x = std::copysign(89.0f, eye_angles.x);
}*/