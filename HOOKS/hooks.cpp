#include "..\includes.h"
#include "../ImGui/imgui.h"
#include "../ImGui/imgui_internal.h"
#include "../ImGui/dx9/imgui_impl_dx9.h"
#include "hooks.h"
#include "../UTILS/interfaces.h"
#include "../UTILS/offsets.h"
#include "../UTILS/NetvarHookManager.h"
#include "../UTILS/render.h"
#include "../FEATURES/NewEventLog.h"
#include "../SDK/CInput.h"
#include "../SDK/IClient.h"
#include "../SDK/CPanel.h"
#include "../SDK/ConVar.h"
#include "../SDK/CGlowObjectManager.h"
#include "../imgui_render.h"
#include "../SDK/IEngine.h"
#include "../SDK/CTrace.h"
#include "../SDK/CClientEntityList.h"
#include "../SDK/RecvData.h"
#include "../SDK/CBaseAnimState.h"
#include "../SDK/ModelInfo.h"
#include "../SDK/ModelRender.h"
#include "../SDK/RenderView.h"
#include "../SDK/CTrace.h"
#include "../SDK/CViewSetup.h"
#include "../SDK/CGlobalVars.h"
#include "../SDK/CPrediction.h"
#include "..\hitmarker.h"
#include "../FEATURES/Movement.h"
#include "../FEATURES/Visuals.h"
#include "../FEATURES/Chams.h"
#include "../FEATURES/AntiAim.h"
#include "../FEATURES/Aimbot.h"
#include "../FEATURES/Backtracking.h"
#include "../FEATURES/FakeWalk.h"
#include "../FEATURES/FakeLag.h"
#include "../FEATURES/EnginePred.h"
#include "../FEATURES/EventListener.h"
#include "../FEATURES/GrenadePrediction.h"
#include "../FEATURES/Legitbot.h"
#include "../FEATURES/Flashlight.h"
#include "../FEATURES/GloveChanger.h"
#include "../FEATURES/SkinChanger.h"
#include "../FEATURES/D9Visuals.h"
#include "..\night_mode.h"
#include "..\FEATURES\custom_font.h"
#include "../shit.h"
#include <intrin.h>
#pragma comment (lib, "d3d9.lib")
#pragma comment (lib, "d3dx9.lib")
namespace INIT
{
	HMODULE Dll;
	HWND Window;
	WNDPROC OldWindow;
}

namespace MenuTabs
{
	void Tab1();
	void Tab2();
	void Tab3();
	void Tab4();
	void Tab5();
}

static bool menu_open = false;
static bool d3d_init = false;
bool PressedKeys[256] = {};
const char* merixids[] =
{
	"1","2","3","4","5","6", "7", "8", "9",
	"Q","W","E","R","T","Y","U","I","O","P",
	"A","S","D","F","G","H","J","K","L",
	"Z","X","C","V","B","N","M",".","\\","|", "/","}","{","[","]",
	"<",">","?","'"
};
static char ConfigNamexd[64] = { 0 };
namespace ImGui
{

	static auto vector_getterxd = [](void* vec, int idx, const char** out_text)
	{
		auto& vector = *static_cast<std::vector<std::string>*>(vec);
		if (idx < 0 || idx >= static_cast<int>(vector.size())) { return false; }
		*out_text = vector.at(idx).c_str();
		return true;
	};

	IMGUI_API bool ComboBoxArrayxd(const char* label, int* currIndex, std::vector<std::string>& values)
	{
		if (values.empty()) { return false; }
		return Combo(label, currIndex, vector_getterxd,
			static_cast<void*>(&values), values.size());
	}
}
ImFont* bigmenu_font;
ImFont* menu_font;
ImFont* smallmenu_font;
ImFont* font_menu;
//--- Other Globally Used Variables ---///
static bool tick = false;
static int ground_tick;
Vector vecAimPunch, vecViewPunch;
Vector* pAimPunch = nullptr;
Vector* pViewPunch = nullptr;

//--- Declare Signatures and Patterns Here ---///
static auto CAM_THINK = UTILS::FindSignature("client_panorama.dll", "85 C0 75 30 38 86");
static auto linegoesthrusmoke = UTILS::FindPattern("client_panorama.dll", (PBYTE)"\x55\x8B\xEC\x83\xEC\x08\x8B\x15\x00\x00\x00\x00\x0F\x57\xC0", "xxxxxxxx????xxx");

//--- Tick Counting ---//
void ground_ticks()
{
	auto local_player = g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer());

	if (!local_player)
		return;

	if (local_player->GetFlags() & FL_ONGROUND)
		ground_tick++;
	else
		ground_tick = 0;
}

namespace HOOKS
{
	CreateMoveFn original_create_move;
	PaintTraverseFn original_paint_traverse;
	PaintFn original_paint;
	FrameStageNotifyFn original_frame_stage_notify;
	DrawModelExecuteFn original_draw_model_execute;
	SceneEndFn original_scene_end;
	TraceRayFn original_trace_ray;
	OverrideViewFn original_override_view;
	RenderViewFn original_render_view;
	SvCheatsGetBoolFn original_get_bool;
	GetViewmodelFOVFn original_viewmodel_fov;

	vfunc_hook fireevent;
	vfunc_hook directz;

	VMT::VMTHookManager iclient_hook_manager;
	VMT::VMTHookManager panel_hook_manager;
	VMT::VMTHookManager paint_hook_manager;
	VMT::VMTHookManager model_render_hook_manager;
	VMT::VMTHookManager scene_end_hook_manager;
	VMT::VMTHookManager render_view_hook_manager;
	VMT::VMTHookManager trace_hook_manager;
	VMT::VMTHookManager net_channel_hook_manager;
	VMT::VMTHookManager override_view_hook_manager;
	VMT::VMTHookManager input_table_manager;
	VMT::VMTHookManager get_bool_manager;
	std::string sPanel = ("FocusOverlayPanel");

	auto get_max_desync_delta = [](void) {

		auto local_player = static_cast<SDK::CBaseEntity*>(g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer()));

		auto animstate = local_player->GetAnimState();

		float duckammount = *(float *)(animstate + 0xA4);
		float speedfraction = max(0, min(*reinterpret_cast<float*>(animstate + 0xF8), 1));

		float speedfactor = max(0, min(1, *reinterpret_cast<float*> (animstate + 0xFC)));

		float unk1 = ((*reinterpret_cast<float*> (animstate + 0x11C) * -0.30000001) - 0.19999999) * speedfraction;
		float unk2 = unk1 + 1.f;

		if (duckammount > 0)
			unk2 += ((duckammount * speedfactor) * (0.5f - unk2)); 

		return *(float *)(animstate + 0x334) * unk2;
	};

	auto get_fixed_feet_yaw = [](void) {

		auto local_player = static_cast<SDK::CBaseEntity*>(g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer()));

		auto state = local_player->GetAnimState();

		float fixed_feet_yaw = -360;
		float current_feet_yaw = *(float *)(state + 0x80);

		if (current_feet_yaw >= -360)
			current_feet_yaw = min(current_feet_yaw, 360.0);

		return fixed_feet_yaw;
	};

	bool __stdcall HookedCreateMove(float sample_input_frametime, SDK::CUserCmd* cmd)
	{
		if (!cmd || cmd->command_number == 0)
			return false;

		uintptr_t* FPointer; __asm { MOV FPointer, EBP }
		byte* SendPacket = (byte*)(*FPointer - 0x1C);
		if (!SendPacket) return false;

		auto local_player = g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer());
		if (!local_player) return false;

		GLOBAL::should_send_packet = *SendPacket;
		GLOBAL::originalCMD = *cmd;
		if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame())
		{
			//GrenadePrediction::instance().Tick(cmd->buttons);
			if (Settings::Options.aim_type == 0)
				slidebitch->do_fakewalk(cmd);

			cmd->buttons |= IN_BULLRUSH;

			if (!g_csgo::Engine->IsVoiceRecording())
				fakelag->do_fakelag(cmd);

			if (Settings::Options.bhop_bool) movement->bunnyhop(cmd);
			if (Settings::Options.duck_bool) movement->duckinair(cmd);

			if (Settings::Options.misc_clantag) visuals->Clantag();
			prediction->run_prediction(cmd); {

				if (Settings::Options.strafe_bool) movement->autostrafer(cmd);

				if (Settings::Options.aim_type == 0 && Settings::Options.aim_bool)
				{
					aimbot->run_aimbot(cmd);
					backtracking->backtrack_player(cmd);
				}

				if (Settings::Options.aim_type == 1 && Settings::Options.aim_bool)
				{
					if (GetAsyncKeyState(UTILS::INPUT::input_handler.keyBindings(Settings::Options.legittrigger_key)) && Settings::Options.legittrigger_bool)
						legitbot->triggerbot(cmd);
					backtracking->run_legit(cmd);
				}

				if (Settings::Options.aa_bool)
				{
					antiaim->do_antiaim(cmd);
					antiaim->fix_movement(cmd);
					//ground_ticks();
				}
			} prediction->end_prediction(cmd);

			if (!GLOBAL::should_send_packet)
				GLOBAL::real_angles = cmd->viewangles;
			else
			{
				GLOBAL::FakePosition = local_player->GetAbsOrigin();
				GLOBAL::fake_angles = cmd->viewangles;
			}

			auto max = get_max_desync_delta();

			//cmd->viewangles.y = max;

			//GLOBAL::fake_angles = cmd->viewangles;

		}
		*SendPacket = GLOBAL::should_send_packet;
		cmd->move = antiaim->fix_movement(cmd, GLOBAL::originalCMD);
		if (Settings::Options.aa_pitch < 2 || Settings::Options.aa_pitch1_type < 2 || Settings::Options.aa_pitch2_type < 2)
			UTILS::ClampLemon(cmd->viewangles);

		return false;
	}

	void render_scope() {

		auto local_player = g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer());
		if (!local_player) return;

		int
			screen_x,
			screen_y,
			center_x,
			center_y;

		g_csgo::Engine->GetScreenSize(screen_x, screen_y); g_csgo::Engine->GetScreenSize(center_x, center_y);
		center_x /= 2; center_y /= 2;

		static float rainbow;
		rainbow += 0.000001;

		if (rainbow > 1.f)
			rainbow = 0.f;

		if (local_player->GetIsScoped()) {

			RENDER::DrawLine(0, center_y, screen_x, center_y, CColor::FromHSB(rainbow, 1.f, 1.f));
			RENDER::DrawLine(center_x, 0, center_x, screen_y, CColor::FromHSB(rainbow, 1.f, 1.f));
		}
	}

	void __stdcall HookedPaintTraverse(int VGUIPanel, bool ForceRepaint, bool AllowForce)
	{
		if (Settings::Options.scope_bool) render_scope();

		std::string panel_name = g_csgo::Panel->GetName(VGUIPanel);
		if (panel_name == "HudZoom" && Settings::Options.scope_bool) return;
		if (panel_name == "FocusOverlayPanel")
		{
			if (FONTS::ShouldReloadFonts())
				FONTS::InitFonts();

			if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame())
			{
				//GrenadePrediction::get().Paint();
				FEATURES::MISC::in_game_logger.Do();
				auto matpostprocess = g_csgo::cvar->FindVar("mat_postprocess_enable");
				matpostprocess->fnChangeCallback = 0;
				matpostprocess->SetValue(Settings::Options.matpostprocessenable);

				visuals->ModulateWorld();

				if (Settings::Options.esp_bool)
				{
					cnight_mode nightmode;

					nightmode.do_nightmode();
					visuals->Draw();
					visuals->ClientDraw();
					visuals->Drawmodels();
				}
				Flashlight.RunFrame();
			}

			/*MENU::PPGUI_PP_GUI::Begin();
			MENU::Do();
			MENU::PPGUI_PP_GUI::End();

			UTILS::INPUT::input_handler.Update();*/



			//	visuals->LogEvents();
		}


		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 4; j++)
			{
				viewMatrix[i][j] = g_csgo::Engine->WorldToScreenMatrix()[i][j];
			}
		}
		original_paint_traverse(g_csgo::Panel, VGUIPanel, ForceRepaint, AllowForce);

		const char* pszPanelName = g_csgo::Panel->GetName(VGUIPanel);

		if (!strstr(pszPanelName, sPanel.data()))
			return;


		g_csgo::Panel->SetMouseInputEnabled(VGUIPanel, menu_open);
	}

	/*void animation_fix_local_player() {

		if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame()) {

			auto local_player = static_cast<SDK::CBaseEntity*>(g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer()));
			auto animations = local_player->GetAnimState();

			if (!animations)
				return;

			if (!local_player)
				return;

			local_player->m_bClientSideAnimation() = true;

			auto old_curtime = g_csgo::Globals->curtime;
			auto old_frametime = g_csgo::Globals->frametime;
			auto old_ragpos = local_player->get_ragdoll_pos();
			auto old_fraction = local_player->GetAnimState()->m_flUnknownFraction = 0.f;

			g_csgo::Globals->curtime = local_player->GetSimTime();
			g_csgo::Globals->frametime = g_csgo::Globals->interval_per_tick;

			auto player_animation_state = reinterpret_cast<DWORD*>(local_player + 0x3894);
			auto player_model_time = reinterpret_cast<int*>(player_animation_state + 112);

			if (player_animation_state != nullptr && player_model_time != nullptr) {

				if (*player_model_time == g_csgo::Globals->framecount)
					*player_model_time = g_csgo::Globals->framecount - 1;
			}

			local_player->get_ragdoll_pos() = old_ragpos;
			local_player->UpdateClientSideAnimation();

			g_csgo::Globals->curtime = old_curtime;
			g_csgo::Globals->frametime = old_frametime;

			local_player->SetAbsAngles(Vector(0.f, local_player->GetAnimState()->m_flGoalFeetYaw, 0.f));

			local_player->m_bClientSideAnimation() = false;
		}
	}*/

	/*void NotReallyUsefull()
	{
		if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame())
		{
			auto local_player = g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer());
			auto animations = local_player->GetAnimState();

			if (!animations)
				return;
			if (!local_player)
				return;

			local_player->m_bClientSideAnimation() = true;

			auto old_curtime = g_csgo::Globals->curtime;
			auto old_frametime = g_csgo::Globals->frametime;
			auto old_ragpos = local_player->get_ragdoll_pos();
			g_csgo::Globals->curtime = local_player->GetSimTime();
			g_csgo::Globals->frametime = g_csgo::Globals->interval_per_tick;
			auto player_animation_state = reinterpret_cast<DWORD*>(local_player + 0x3894);
			auto player_model_time = reinterpret_cast<int*>(player_animation_state + 112);
			if (player_animation_state != nullptr && player_model_time != nullptr)
				if (*player_model_time == g_csgo::Globals->framecount)
					*player_model_time = g_csgo::Globals->framecount - 1;

			local_player->get_ragdoll_pos() = old_ragpos;
			local_player->UpdateClientSideAnimation();

			g_csgo::Globals->curtime = old_curtime;
			g_csgo::Globals->frametime = old_frametime;

			local_player->SetAbsAngles(Vector(0.f, local_player->GetAnimState()->m_flGoalFeetYaw, 0.f));

			local_player->m_bClientSideAnimation() = false;
		}
	}*/

	void __fastcall HookedFrameStageNotify(void* ecx, void* edx, int stage)
	{

		auto local_player = g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer());
		if (!local_player) return;
		Vector vecAimPunch, vecViewPunch;
		Vector* pAimPunch = nullptr; Vector* pViewPunch = nullptr;

		auto GetCorrectDistance = [&local_player](float ideal_distance) -> float {

			Vector inverse_angles;
			g_csgo::Engine->GetViewAngles(inverse_angles);

			inverse_angles.x *= -1.f, inverse_angles.y += 180.f;

			Vector direction;
			MATH::AngleVectors(inverse_angles, &direction);

			SDK::CTraceWorldOnly filter;
			SDK::trace_t trace;
			SDK::Ray_t ray;

			ray.Init(local_player->GetVecOrigin() + local_player->GetViewOffset(), (local_player->GetVecOrigin() + local_player->GetViewOffset()) + (direction * (ideal_distance + 5.f)));
			g_csgo::Trace->TraceRay(ray, MASK_ALL, &filter, &trace);

			return ideal_distance * trace.flFraction;
		};

		switch (stage)
		{
		case FRAME_NET_UPDATE_POSTDATAUPDATE_START:
			if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame())
			{
				GloveChanger();
				xdSkinchanger();
				for (int i = 1; i <= 65; i++)
				{
					auto entity = g_csgo::ClientEntityList->GetClientEntity(i);
					if (!entity) continue;

					bool is_local_player = entity == local_player;
					bool is_teammate = local_player->GetTeam() == entity->GetTeam() && !is_local_player;

					if (is_local_player) continue;
					if (is_teammate) continue;
					if (entity->GetHealth() <= 0) continue;
					if (entity->GetIsDormant()) continue;

					//player_resolver resolver;
					// new resolvo
					//resolver.run();
				}
			} break;
		case FRAME_NET_UPDATE_POSTDATAUPDATE_END:
			break;
		case FRAME_RENDER_START:
			if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame())
			{
				if (in_tp)
				{

					*(Vector*)((DWORD)local_player + 0x31C8) = Vector(GLOBAL::real_angles.x, GLOBAL::real_angles.y, 0.f);

					g_csgo::pPrediction->SetLocalViewAngles(GLOBAL::real_angles);
					local_player->UpdateClientSideAnimation();
					g_csgo::pPrediction->SetLocalViewAngles(GLOBAL::real_angles);

				}
				for (int i = 1; i <= 65; i++)
				{
					auto entity = g_csgo::ClientEntityList->GetClientEntity(i);
					if (!entity) continue;
					if (entity == local_player) continue;

					*(int*)((uintptr_t)entity + 0xA30) = g_csgo::Globals->framecount;
					*(int*)((uintptr_t)entity + 0xA28) = 0;
				}
			} break;

		case FRAME_NET_UPDATE_START:
			if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame())
			{
				if (Settings::Options.beam_bool)
					visuals->DrawBulletBeams();
			} break;
		case FRAME_NET_UPDATE_END:
			if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame())
			{
				for (int i = 1; i < 65; i++)
				{
					auto entity = g_csgo::ClientEntityList->GetClientEntity(i);

					if (!entity)
						continue;

					if (!local_player)
						continue;

					bool is_local_player = entity == local_player;
					bool is_teammate = local_player->GetTeam() == entity->GetTeam() && !is_local_player;

					if (is_local_player)
						continue;

					if (is_teammate)
						continue;

					if (entity->GetHealth() <= 0)
						continue;

					if (Settings::Options.aim_type == 0)
						backtracking->DisableInterpolation(entity);
				}
			}
			break;
		case FRAME_RENDER_END:
			if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame()) {

			}
		}
		original_frame_stage_notify(ecx, stage);
	}

	void __fastcall HookedDrawModelExecute(void* ecx, void* edx, SDK::IMatRenderContext* context, const SDK::DrawModelState_t& state, const SDK::ModelRenderInfo_t& render_info, matrix3x4_t* matrix)
	{
		if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame())
		{
			std::string ModelName = g_csgo::ModelInfo->GetModelName(render_info.pModel);

			auto local_player = static_cast<SDK::CBaseEntity*>(g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer()));

			if (in_tp && render_info.entity_index == local_player->GetIndex() && local_player->GetIsScoped())
				g_csgo::RenderView->SetBlend(Settings::Options.transparency_amnt);

			if (ModelName.find("v_sleeve") != std::string::npos)
			{
				SDK::IMaterial* material = g_csgo::MaterialSystem->FindMaterial(ModelName.c_str(), TEXTURE_GROUP_MODEL);
				if (!material) return;
				material->SetMaterialVarFlag(SDK::MATERIAL_VAR_NO_DRAW, true);
				g_csgo::ModelRender->ForcedMaterialOverride(material);
			}

		}
		original_draw_model_execute(ecx, context, state, render_info, matrix);
		g_csgo::RenderView->SetBlend(1.f);
	}

	void __fastcall HookedSceneEnd(void* ecx, void* edx)
	{
		original_scene_end(ecx);
		static SDK::IMaterial* ignorez = chams->CreateMaterialBasic(true, true, false);
		static SDK::IMaterial* notignorez = chams->CreateMaterialBasic(false, true, false);
		static SDK::IMaterial* ignorez_metallic = chams->CreateMaterialMetallic(true, true, false);
		static SDK::IMaterial* notignorez_metallic = chams->CreateMaterialMetallic(false, true, false);

		if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame())
		{
			auto local_player = g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer());
			if (!local_player) return;
			CColor color = CColor(Settings::Options.glow_col[0] * 255, Settings::Options.glow_col[1] * 255, Settings::Options.glow_col[2] * 255, Settings::Options.glow_col[3] * 255), colorTeam = CColor(Settings::Options.teamglow_color[0] * 255, Settings::Options.teamglow_color[1] * 255, Settings::Options.teamglow_color[2] * 255, Settings::Options.teamglow_color[3] * 255), colorlocal = CColor(Settings::Options.glowlocal_col[0] * 255, Settings::Options.glowlocal_col[1] * 255, Settings::Options.glowlocal_col[2] * 255, Settings::Options.glowlocal_col[3] * 255);
			for (int i = 1; i < 65; i++)
			{
				if (Settings::Options.fakechams)
				{
					auto pLocal = reinterpret_cast<SDK::CBaseEntity*>(g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer()));
					if (pLocal && pLocal->IsAlive())
					{
						static SDK::IMaterial* mat = chams->CreateMaterialBasic(false, true, false);
						if (mat)
						{
							Vector OrigAng = pLocal->GetEyeAngles();
							pLocal->SetAngle2(Vector(0, GLOBAL::fake_angles.y, 0));
							bool LbyColor = false;
							g_csgo::RenderView->SetColorModulation(CColor(255, 255, 255));
							g_csgo::RenderView->SetBlend(.5f);
							g_csgo::ModelRender->ForcedMaterialOverride(mat);
							pLocal->DrawModel(0x1, 150);
							g_csgo::ModelRender->ForcedMaterialOverride(nullptr);
							pLocal->SetAngle2(OrigAng);
						}
					}
				}

				auto entity = g_csgo::ClientEntityList->GetClientEntity(i);

				if (!entity) continue;
				if (!local_player) continue;

				bool is_local_player = entity == local_player;
				bool is_teammate = local_player->GetTeam() == entity->GetTeam() && !is_local_player;
				auto ignorezmaterial = Settings::Options.chamstype == 0 ? ignorez_metallic : ignorez;
				auto notignorezmaterial = Settings::Options.chamstype == 0 ? notignorez_metallic : notignorez;
				if (is_local_player)
				{
					switch (Settings::Options.localchams)
					{
					case 0: continue; break;
					case 1:
						local_player->SetAbsOrigin(GLOBAL::FakePosition);
						local_player->DrawModel(0x1, 255);
						local_player->SetAbsOrigin(local_player->GetAbsOrigin());
						continue; break;
					case 2:
						g_csgo::RenderView->SetColorModulation(CColor(Settings::Options.localchams_col[0] * 255, Settings::Options.localchams_col[1] * 255, Settings::Options.localchams_col[2] * 255, Settings::Options.localchams_col[3] * 255));
						g_csgo::ModelRender->ForcedMaterialOverride(notignorezmaterial);
						local_player->DrawModel(0x1, 255);
						g_csgo::ModelRender->ForcedMaterialOverride(nullptr);
						continue;  break;
					case 3:
						notignorezmaterial->ColorModulate(CColor(Settings::Options.localchams_col[0] * 255, Settings::Options.localchams_col[1] * 255, Settings::Options.localchams_col[2] * 255, Settings::Options.localchams_col[3] * 255));
						g_csgo::ModelRender->ForcedMaterialOverride(notignorezmaterial);
						local_player->SetAbsOrigin(GLOBAL::FakePosition);
						local_player->DrawModel(0x1, 255);
						local_player->SetAbsOrigin(local_player->GetAbsOrigin());
						g_csgo::ModelRender->ForcedMaterialOverride(nullptr);
						continue; break;
					}
				}

				if (entity->GetHealth() <= 0) continue;
				if (entity->GetIsDormant())	continue;
				if (entity->GetClientClass()->m_ClassID != 38) continue;

				if (is_teammate)
				{
					if (entity && Settings::Options.chamsteam == 2)
					{
						ignorezmaterial->ColorModulate(CColor(Settings::Options.teaminvis_color[0] * 255, Settings::Options.teaminvis_color[1] * 255, Settings::Options.teaminvis_color[2] * 255, Settings::Options.teaminvis_color[3] * 255));
						g_csgo::ModelRender->ForcedMaterialOverride(ignorezmaterial);
						entity->DrawModel(0x1, 255);

						notignorezmaterial->ColorModulate(CColor(Settings::Options.teamvis_color[0] * 255, Settings::Options.teamvis_color[1] * 255, Settings::Options.teamvis_color[2] * 255, Settings::Options.teamvis_color[3] * 255));
						g_csgo::ModelRender->ForcedMaterialOverride(notignorezmaterial);
						entity->DrawModel(0x1, 255);

						g_csgo::ModelRender->ForcedMaterialOverride(nullptr);
					}
					else if (entity && Settings::Options.chamsteam == 1)
					{
						notignorezmaterial->ColorModulate(CColor(Settings::Options.teamvis_color[0] * 255, Settings::Options.teamvis_color[1] * 255, Settings::Options.teamvis_color[2] * 255, Settings::Options.teamvis_color[3] * 255));
						g_csgo::ModelRender->ForcedMaterialOverride(notignorezmaterial);
						entity->DrawModel(0x1, 255);

						g_csgo::ModelRender->ForcedMaterialOverride(nullptr);
					} continue;
				}
				else if (is_teammate && Settings::Options.chamsteam)
					continue;

				if (entity && Settings::Options.chams_type == 2)
				{
					ignorezmaterial->ColorModulate(CColor(Settings::Options.imodel_col[0] * 255, Settings::Options.imodel_col[1] * 255, Settings::Options.imodel_col[2] * 255, Settings::Options.imodel_col[3] * 255));
					g_csgo::ModelRender->ForcedMaterialOverride(ignorezmaterial);
					entity->DrawModel(0x1, 255);

					notignorezmaterial->ColorModulate(CColor(Settings::Options.vmodel_col[0] * 255, Settings::Options.vmodel_col[1] * 255, Settings::Options.vmodel_col[2] * 255, Settings::Options.vmodel_col[3] * 255));
					g_csgo::ModelRender->ForcedMaterialOverride(notignorezmaterial);
					entity->DrawModel(0x1, 255);

					g_csgo::ModelRender->ForcedMaterialOverride(nullptr);
				}
				else if (entity && Settings::Options.chams_type == 1)
				{
					notignorezmaterial->ColorModulate(CColor(Settings::Options.vmodel_col[0] * 255, Settings::Options.vmodel_col[1] * 255, Settings::Options.vmodel_col[2] * 255, Settings::Options.vmodel_col[3] * 255));
					g_csgo::ModelRender->ForcedMaterialOverride(notignorezmaterial);
					entity->DrawModel(0x1, 255);

					g_csgo::ModelRender->ForcedMaterialOverride(nullptr);
				}
			}

			for (auto i = 0; i < g_csgo::GlowObjManager->GetSize(); i++)
			{
				auto &glowObject = g_csgo::GlowObjManager->m_GlowObjectDefinitions[i];
				auto entity = reinterpret_cast<SDK::CBaseEntity*>(glowObject.m_pEntity);

				if (!entity) continue;
				if (!local_player) continue;

				if (glowObject.IsUnused()) continue;

				bool is_local_player = entity == local_player;
				bool is_teammate = local_player->GetTeam() == entity->GetTeam() && !is_local_player;

				if (is_local_player && in_tp && Settings::Options.glowlocal)
				{
					glowObject.m_nGlowStyle = Settings::Options.glowstylelocal;
					glowObject.m_flRed = colorlocal.RGBA[0] / 255.0f;
					glowObject.m_flGreen = colorlocal.RGBA[1] / 255.0f;
					glowObject.m_flBlue = colorlocal.RGBA[2] / 255.0f;
					glowObject.m_flAlpha = colorlocal.RGBA[3] / 255.0f;
					glowObject.m_bRenderWhenOccluded = true;
					glowObject.m_bRenderWhenUnoccluded = false;
					continue;
				}
				else if (!Settings::Options.glowlocal && is_local_player)
					continue;

				if (entity->GetHealth() <= 0) continue;
				if (entity->GetIsDormant())	continue;
				if (entity->GetClientClass()->m_ClassID != 38) continue;

				if (is_teammate && Settings::Options.glowteam)
				{
					glowObject.m_nGlowStyle = Settings::Options.glowstyle; //0;
					glowObject.m_flRed = colorTeam.RGBA[0] / 255.0f;
					glowObject.m_flGreen = colorTeam.RGBA[1] / 255.0f;
					glowObject.m_flBlue = colorTeam.RGBA[2] / 255.0f;
					glowObject.m_flAlpha = colorTeam.RGBA[3] / 255.0f;
					glowObject.m_bRenderWhenOccluded = true;
					glowObject.m_bRenderWhenUnoccluded = false;
					continue;
				}
				else if (is_teammate && !Settings::Options.glowteam)
					continue;

				if (Settings::Options.glowenable)
				{
					glowObject.m_nGlowStyle = Settings::Options.glowstyle;//0;
					glowObject.m_flRed = color.RGBA[0] / 255.0f;
					glowObject.m_flGreen = color.RGBA[1] / 255.0f;
					glowObject.m_flBlue = color.RGBA[2] / 255.0f;
					glowObject.m_flAlpha = color.RGBA[3] / 255.0f;
					glowObject.m_bRenderWhenOccluded = true;
					glowObject.m_bRenderWhenUnoccluded = false;
				}
			}

			SDK::IMaterial* flash = g_csgo::MaterialSystem->FindMaterial("effects/flashbang", TEXTURE_GROUP_CLIENT_EFFECTS);
			SDK::IMaterial* flashWhite = g_csgo::MaterialSystem->FindMaterial("effects/flashbang_white", TEXTURE_GROUP_CLIENT_EFFECTS);

			flash->SetMaterialVarFlag(SDK::MATERIAL_VAR_NO_DRAW, Settings::Options.no_flash ? true : false);
			flashWhite->SetMaterialVarFlag(SDK::MATERIAL_VAR_NO_DRAW, Settings::Options.no_flash ? true : false);

			if (Settings::Options.smoke_bool)
			{
				std::vector<const char*> vistasmoke_wireframe = { "particle/vistasmokev1/vistasmokev1_smokegrenade" };

				std::vector<const char*> vistasmoke_nodraw =
				{
					"particle/vistasmokev1/vistasmokev1_fire",
					"particle/vistasmokev1/vistasmokev1_emods",
					"particle/vistasmokev1/vistasmokev1_emods_impactdust",
				};

				for (auto mat_s : vistasmoke_wireframe)
				{
					SDK::IMaterial* mat = g_csgo::MaterialSystem->FindMaterial(mat_s, TEXTURE_GROUP_OTHER);
					mat->SetMaterialVarFlag(SDK::MATERIAL_VAR_WIREFRAME, true); //wireframe
				}

				for (auto mat_n : vistasmoke_nodraw)
				{
					SDK::IMaterial* mat = g_csgo::MaterialSystem->FindMaterial(mat_n, TEXTURE_GROUP_OTHER);
					mat->SetMaterialVarFlag(SDK::MATERIAL_VAR_NO_DRAW, true);
				}

				static auto smokecout = *(DWORD*)(linegoesthrusmoke + 0x8);
				*(int*)(smokecout) = 0;
			}
		}
	}
	void __fastcall HookedOverrideView(void* ecx, void* edx, SDK::CViewSetup* pSetup)
	{
		auto local_player = g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer());
		if (!local_player) return;

		auto animstate = local_player->GetAnimState();
		if (!animstate) return;

		if (GetAsyncKeyState(UTILS::INPUT::input_handler.keyBindings(Settings::Options.thirdperson_int)) & 1)
			in_tp = !in_tp;

		if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame())
		{
			//GrenadePrediction::instance().View(pSetup);
			auto GetCorrectDistance = [&local_player](float ideal_distance) -> float //lambda
			{
				Vector inverse_angles;
				g_csgo::Engine->GetViewAngles(inverse_angles);

				inverse_angles.x *= -1.f, inverse_angles.y += 180.f;

				Vector direction;
				MATH::AngleVectors(inverse_angles, &direction);

				SDK::CTraceWorldOnly filter;
				SDK::trace_t trace;
				SDK::Ray_t ray;

				ray.Init(local_player->GetVecOrigin() + local_player->GetViewOffset(), (local_player->GetVecOrigin() + local_player->GetViewOffset()) + (direction * (ideal_distance + 5.f)));
				g_csgo::Trace->TraceRay(ray, MASK_ALL, &filter, &trace);

				return ideal_distance * trace.flFraction;
			};

			if (Settings::Options.tp_bool && in_tp) {
				
				if (!local_player->IsAlive())
					local_player->SetObserverMode(5);

				auto weapon = reinterpret_cast<SDK::CBaseWeapon*>(g_csgo::ClientEntityList->GetClientEntity(local_player->GetActiveWeaponIndex())); if (!weapon) return;

				if (weapon->is_grenade())
					g_csgo::Input->m_fCameraInThirdPerson = false;

				if (!g_csgo::Input->m_fCameraInThirdPerson) {

					g_csgo::Input->m_fCameraInThirdPerson = true;
					g_csgo::Input->m_vecCameraOffset = Vector(GLOBAL::real_angles.x, GLOBAL::real_angles.y, GetCorrectDistance(100));

					Vector camForward;
					MATH::AngleVectors(Vector(g_csgo::Input->m_vecCameraOffset.x, g_csgo::Input->m_vecCameraOffset.y, 0), &camForward);
				}
			}
			else {
				g_csgo::Input->m_fCameraInThirdPerson = false;
				g_csgo::Input->m_vecCameraOffset = Vector(GLOBAL::real_angles.x, GLOBAL::real_angles.y, 0);
			}

			auto zoomsensration = g_csgo::cvar->FindVar("zoom_sensitivity_ratio_mouse");
			if (Settings::Options.fixscopesens)
				zoomsensration->SetValue("0");
			else
				zoomsensration->SetValue("1");

			if (Settings::Options.aim_type == 0) {

				if (!local_player->GetIsScoped())
					pSetup->fov = Settings::Options.fov_val;
				else if (local_player->GetIsScoped() && Settings::Options.removescoping)
					pSetup->fov = Settings::Options.fov_val;
			}
			else if (!(Settings::Options.aim_type == 0) && !local_player->GetIsScoped())
				pSetup->fov = 90;
		}
		original_override_view(ecx, pSetup);
	}
	void __fastcall HookedTraceRay(void *thisptr, void*, const SDK::Ray_t &ray, unsigned int fMask, SDK::ITraceFilter *pTraceFilter, SDK::trace_t *pTrace)
	{
		original_trace_ray(thisptr, ray, fMask, pTraceFilter, pTrace);
		if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame())
			pTrace->surface.flags |= SURF_SKY;
	}
	bool __fastcall HookedGetBool(void* pConVar, void* edx)
	{
		if ((uintptr_t)_ReturnAddress() == CAM_THINK)
			return true;

		return original_get_bool(pConVar);
	}
	float __fastcall GetViewmodelFOV()
	{
		if (g_csgo::Engine->IsConnected() && g_csgo::Engine->IsInGame())
		{
			float player_fov = original_viewmodel_fov();

			if (Settings::Options.esp_bool)
				player_fov = Settings::Options.viewfov_val;

			return player_fov;
		}
	}






	void OpenMenu()
	{
		static bool is_down = false;
		static bool is_clicked = false;
		if (GetAsyncKeyState(VK_INSERT))
		{
			is_clicked = false;
			is_down = true;
		}
		else if (!GetAsyncKeyState(VK_INSERT) && is_down)
		{
			is_clicked = true;
			is_down = false;
		}
		else
		{
			is_clicked = false;
			is_down = false;
		}

		if (is_clicked)
		{
			menu_open = !menu_open;

		}
	}




	LRESULT __stdcall Hooked_WndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
	{
		switch (uMsg) {
		case WM_LBUTTONDOWN:
			PressedKeys[VK_LBUTTON] = true;
			break;
		case WM_LBUTTONUP:
			PressedKeys[VK_LBUTTON] = false;
			break;
		case WM_RBUTTONDOWN:
			PressedKeys[VK_RBUTTON] = true;
			break;
		case WM_RBUTTONUP:
			PressedKeys[VK_RBUTTON] = false;
			break;
		case WM_MBUTTONDOWN:
			PressedKeys[VK_MBUTTON] = true;
			break;
		case WM_MBUTTONUP:
			PressedKeys[VK_MBUTTON] = false;
			break;
		case WM_XBUTTONDOWN:
		{
			UINT button = GET_XBUTTON_WPARAM(wParam);
			if (button == XBUTTON1)
			{
				PressedKeys[VK_XBUTTON1] = true;
			}
			else if (button == XBUTTON2)
			{
				PressedKeys[VK_XBUTTON2] = true;
			}
			break;
		}
		case WM_XBUTTONUP:
		{
			UINT button = GET_XBUTTON_WPARAM(wParam);
			if (button == XBUTTON1)
			{
				PressedKeys[VK_XBUTTON1] = false;
			}
			else if (button == XBUTTON2)
			{
				PressedKeys[VK_XBUTTON2] = false;
			}
			break;
		}
		case WM_KEYDOWN:
			PressedKeys[wParam] = true;
			break;
		case WM_KEYUP:
			PressedKeys[wParam] = false;
			break;
		default: break;
		}

		OpenMenu();

		if (d3d_init && menu_open && ImGui_ImplDX9_WndProcHandler(hWnd, uMsg, wParam, lParam))
			return true;

		return CallWindowProc(INIT::OldWindow, hWnd, uMsg, wParam, lParam);
	}


	void GUI_Init(IDirect3DDevice9* pDevice)
	{
		ImGui_ImplDX9_Init(INIT::Window, pDevice);


		ImGuiIO& io = ImGui::GetIO();
		ImGuiStyle& style = ImGui::GetStyle();
		io.Fonts->AddFontFromFileTTF("C:\\Windows\\Fonts\\Arial.ttf", 15.0f);


		ImGuiStyle &styled = ImGui::GetStyle();
		ImVec4* colors = ImGui::GetStyle().Colors;
		colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
		colors[ImGuiCol_TextDisabled] = ImVec4(0.60f, 0.60f, 0.60f, 1.00f);
		colors[ImGuiCol_WindowBg] = ImVec4(0.45f, 0.00f, 0.29f, 1.00f);
		colors[ImGuiCol_ChildWindowBg] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_PopupBg] = ImVec4(0.45f, 0.00f, 0.29f, 1.00f);
		colors[ImGuiCol_Border] = ImVec4(0.71f, 0.00f, 0.45f, 1.00f);
		colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
		colors[ImGuiCol_FrameBg] = ImVec4(1.00f, 1.00f, 1.00f, 0.28f);
		colors[ImGuiCol_FrameBgHovered] = ImVec4(1.00f, 1.00f, 1.00f, 0.28f);
		colors[ImGuiCol_FrameBgActive] = ImVec4(1.00f, 1.00f, 1.00f, 0.28f);
		colors[ImGuiCol_TitleBg] = ImVec4(1.00f, 0.00f, 0.64f, 1.00f);
		colors[ImGuiCol_TitleBgActive] = ImVec4(1.00f, 0.00f, 0.64f, 1.00f);
		colors[ImGuiCol_TitleBgCollapsed] = ImVec4(1.00f, 0.00f, 0.64f, 0.00f);
		colors[ImGuiCol_MenuBarBg] = ImVec4(1.00f, 0.00f, 0.64f, 0.00f);
		colors[ImGuiCol_ScrollbarBg] = ImVec4(0.85f, 0.85f, 0.85f, 0.12f);
		colors[ImGuiCol_ScrollbarGrab] = ImVec4(1.00f, 0.56f, 0.81f, 0.30f);
		colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(1.00f, 1.00f, 1.00f, 0.19f);
		colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.75f, 0.08f, 0.53f, 0.30f);
		colors[ImGuiCol_CheckMark] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
		colors[ImGuiCol_SliderGrab] = ImVec4(1.00f, 1.00f, 1.00f, 0.30f);
		colors[ImGuiCol_SliderGrabActive] = ImVec4(0.75f, 0.08f, 0.53f, 0.30f);
		colors[ImGuiCol_Button] = ImVec4(1.00f, 1.00f, 1.00f, 0.28f);
		colors[ImGuiCol_ButtonHovered] = ImVec4(1.00f, 1.00f, 1.00f, 0.49f);
		colors[ImGuiCol_ButtonActive] = ImVec4(0.71f, 0.71f, 0.71f, 0.61f);
		colors[ImGuiCol_Header] = ImVec4(1.00f, 0.00f, 0.70f, 0.77f);
		colors[ImGuiCol_HeaderHovered] = ImVec4(1.00f, 0.00f, 0.70f, 0.57f);
		colors[ImGuiCol_HeaderActive] = ImVec4(1.00f, 0.00f, 0.70f, 0.38f);
		colors[ImGuiCol_Separator] = ImVec4(0.71f, 0.00f, 0.45f, 1.00f);
		colors[ImGuiCol_SeparatorHovered] = ImVec4(0.71f, 0.00f, 0.45f, 1.00f);
		colors[ImGuiCol_SeparatorActive] = ImVec4(0.71f, 0.00f, 0.45f, 1.00f);
		colors[ImGuiCol_ResizeGrip] = ImVec4(1.00f, 1.00f, 1.00f, 0.30f);
		colors[ImGuiCol_ResizeGripHovered] = ImVec4(1.00f, 1.00f, 1.00f, 0.60f);
		colors[ImGuiCol_ResizeGripActive] = ImVec4(1.00f, 1.00f, 1.00f, 0.90f);
		colors[ImGuiCol_PlotLines] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
		colors[ImGuiCol_PlotLinesHovered] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
		colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
		colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
		colors[ImGuiCol_TextSelectedBg] = ImVec4(0.00f, 0.00f, 1.00f, 0.35f);

		style.WindowRounding = 7.f;

		/*style.WindowPadding = ImVec2(32, 32);
		style.WindowRounding = 5.0f;
		style.WindowMinSize = ImVec2(32, 32);
		style.WindowTitleAlign = ImVec2(0.5f, 0.5f);
		style.ChildWindowRounding = 0.0f;
		style.FramePadding = ImVec2(5, 5);
		style.FrameRounding = 4.0f;
		style.ItemSpacing = ImVec2(12, 8);
		style.ItemInnerSpacing = ImVec2(8, 6);
		style.IndentSpacing = 25.0f;
		style.ScrollbarSize = 15.0f;
		style.ScrollbarRounding = 9.0f;
		style.GrabMinSize = 5.0f;
		style.GrabRounding = 3.0f;
		style.ButtonTextAlign = ImVec2(0.5f, 0.5f);
		style.DisplayWindowPadding = ImVec2(22, 22);
		style.DisplaySafeAreaPadding = ImVec2(4, 4);
		style.AntiAliasedLines = true;
		style.AntiAliasedShapes = true;
		style.CurveTessellationTol = 1.25f;*/

		pDevice->GetViewport(&Menuxd::viewPort);
		D3DXCreateFont(pDevice, 9, 0, FW_BOLD, 1, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DRAFT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, ("Verdana"), &Menuxd::fntVerdana9);
		D3DXCreateFont(pDevice, 10, 5, FW_NORMAL, 1, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, ("Verdana"), &Menuxd::fntVerdana10);
		D3DXCreateFont(pDevice, 11, 5, FW_NORMAL, 1, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, ("Verdana"), &Menuxd::fntVerdana11);
		//D3DXCreateFont(pDevice, 12, 5, FW_BOLD, 1, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, ("Verdana"), &Menuxd::fntVerdana12);
		D3DXCreateFont(pDevice, 11, 0, FW_NORMAL, 1, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, ("csgo_icons"), &Menuxd::fntWeaponIcon);
		bigmenu_font = ImGui::GetIO().Fonts->AddFontFromMemoryCompressedBase85TTF(rawData_compressed_data_base85, 70);
		menu_font = ImGui::GetIO().Fonts->AddFontFromMemoryCompressedBase85TTF(rawData_compressed_data_base85, 18);
		smallmenu_font = ImGui::GetIO().Fonts->AddFontFromMemoryCompressedBase85TTF(smalll_compressed_data_base85, 13);
		font_menu = ImGui::GetIO().Fonts->AddFontFromMemoryTTF(gamer, 34952, 24.f);
		d3d_init = true;
	}
	long __stdcall Hooked_EndScene(IDirect3DDevice9* pDevice)
	{
		static auto ofunc = directz.get_original<EndSceneFn>(42);

		D3DCOLOR rectColor = D3DCOLOR_XRGB(255, 0, 0);
		D3DRECT BarRect = { 1, 1, 1, 1 };
		ImGuiStyle& style = ImGui::GetStyle();
		pDevice->Clear(1, &BarRect, D3DCLEAR_TARGET | D3DCLEAR_TARGET, rectColor, 0, 0);

		if (!d3d_init)
			GUI_Init(pDevice);

		//ImGui::GetIO().MouseDrawCursor = menu_open;

		static const D3DRENDERSTATETYPE backupStates[] = { D3DRS_COLORWRITEENABLE, D3DRS_ALPHABLENDENABLE, D3DRS_SRCBLEND, D3DRS_DESTBLEND, D3DRS_BLENDOP, D3DRS_FOGENABLE };
		static const int size = sizeof(backupStates) / sizeof(DWORD);

		DWORD oldStates[size] = { 0 };

		for (int i = 0; i < size; i++)
			pDevice->GetRenderState(backupStates[i], &oldStates[i]);

		pDevice->SetRenderState(D3DRS_COLORWRITEENABLE, 0xFFFFFFFF);

		ImGui_ImplDX9_NewFrame();

		if (menu_open) {

			static int MenuTab = 0;
			ImVec2 mainWindowPos;
			ImGui::SetNextWindowPosCenter(ImGuiSetCond_Appearing);
			ImGui::SetNextWindowSize(ImVec2(1010, 665)); // setting window size like this else imgui is gay
			
			ImGui::Begin("wanheda sdk", &menu_open, ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoScrollbar); {

				mainWindowPos = ImGui::GetWindowPos();

				ImGui::PushFont(font_menu);

				if (ImGui::Button("RAGE BOT", ImVec2(192, 50)))
					MenuTab = 0;
				ImGui::SameLine();
				if (ImGui::Button("VISUALS", ImVec2(192, 50)))
					MenuTab = 1;
				ImGui::SameLine();
				if (ImGui::Button("MISC", ImVec2(192, 50)))
					MenuTab = 2;
				ImGui::SameLine();
				if (ImGui::Button("SKINS", ImVec2(192, 50)))
					MenuTab = 3;
				ImGui::SameLine();
				if (ImGui::Button("CONFIGS", ImVec2(192, 50)))
					MenuTab = 4;

				ImGui::PopFont();

				ImGui::BeginChild("##Child", ImVec2(-1, -1), true, ImGuiWindowFlags_NoTitleBar);

				switch (MenuTab) {

					case 0: MenuTabs::Tab1(); break;
					case 1: MenuTabs::Tab2(); break;
					case 2: MenuTabs::Tab3(); break;
					case 3: MenuTabs::Tab4(); break;
					case 4: MenuTabs::Tab5(); break;
				} ImGui::EndChild();

			} ImGui::End();
		} ImGui::Render();

		if (Settings::Options.Visuals.Enabled)
			D9Visuals::Render(pDevice);

		if (Settings::Options.spread_bool == 3)
			Menuxd::drawfatalpricel(pDevice);

		for (int i = 0; i < size; i++)
			pDevice->SetRenderState(backupStates[i], oldStates[i]);

		return ofunc(pDevice);

	}

	HRESULT __stdcall hooked_present(IDirect3DDevice9* device, const RECT *pSourceRect, const RECT *pDestRect, HWND hDestWindowOverride, const RGNDATA *pDirtyRegion) {

		static auto ofunc = directz.get_original<hooked_presentFn>(17);

		return ofunc(device, pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);
	}

	long __stdcall Hooked_EndScene_Reset(IDirect3DDevice9* pDevice, D3DPRESENT_PARAMETERS* pPresentationParameters)
	{
		static auto ofunc = directz.get_original<EndSceneResetFn>(16);

		if (!d3d_init)
			return ofunc(pDevice, pPresentationParameters);

		ImGui_ImplDX9_InvalidateDeviceObjects();

		auto hr = ofunc(pDevice, pPresentationParameters);

		ImGui_ImplDX9_CreateDeviceObjects();

		return hr;
	}

	typedef bool(__thiscall *FireEventClientSide)(void*, SDK::IGameEvent*);

	bool __fastcall Hooked_FireEventClientSide(void *ecx, void* edx, SDK::IGameEvent* pEvent) {

		static auto ofunct = fireevent.get_original<FireEventClientSide>(9);

		if (!pEvent)
			return ofunct(ecx, pEvent);

		DamageESP::HandleGameEvent(pEvent);

		/*
		if (g_Options.NewLegitbot.Aimbot.bKillDelay && g_Options.NewLegitbot.Aimbot.Enabled)
		{

		if (strcmp(pEvent->GetName(), "player_death") == 0)
		{
		int attacker = Interfaces.pEngine->GetPlayerForUserID(pEvent->GetInt("attacker"));
		int iLocalPlayer = Interfaces.pEngine->GetLocalPlayer();
		if (attacker == iLocalPlayer)
		{
		G::KillStopDelay = Interfaces.pGlobalVars->curtime + g_Options.NewLegitbot.Aimbot.iKillDelay;
		}
		}
		}
		*/

		return ofunct(ecx, pEvent);
	}




	void InitHooks()
	{
		iclient_hook_manager.Init(g_csgo::Client);
		original_frame_stage_notify = reinterpret_cast<FrameStageNotifyFn>(iclient_hook_manager.HookFunction<FrameStageNotifyFn>(37, HookedFrameStageNotify)); // fsn as always fuck you

		panel_hook_manager.Init(g_csgo::Panel);
		original_paint_traverse = reinterpret_cast<PaintTraverseFn>(panel_hook_manager.HookFunction<PaintTraverseFn>(41, HookedPaintTraverse));

		model_render_hook_manager.Init(g_csgo::ModelRender);
		original_draw_model_execute = reinterpret_cast<DrawModelExecuteFn>(model_render_hook_manager.HookFunction<DrawModelExecuteFn>(21, HookedDrawModelExecute));

		scene_end_hook_manager.Init(g_csgo::RenderView);
		original_scene_end = reinterpret_cast<SceneEndFn>(scene_end_hook_manager.HookFunction<SceneEndFn>(9, HookedSceneEnd));

		trace_hook_manager.Init(g_csgo::Trace);
		original_trace_ray = reinterpret_cast<TraceRayFn>(trace_hook_manager.HookFunction<TraceRayFn>(5, HookedTraceRay));

		override_view_hook_manager.Init(g_csgo::ClientMode);
		original_override_view = reinterpret_cast<OverrideViewFn>(override_view_hook_manager.HookFunction<OverrideViewFn>(18, HookedOverrideView));
		original_create_move = reinterpret_cast<CreateMoveFn>(override_view_hook_manager.HookFunction<CreateMoveFn>(24, HookedCreateMove));
		original_viewmodel_fov = reinterpret_cast<GetViewmodelFOVFn>(override_view_hook_manager.HookFunction<GetViewmodelFOVFn>(35, GetViewmodelFOV));

		auto sv_cheats = g_csgo::cvar->FindVar("sv_cheats");
		get_bool_manager = VMT::VMTHookManager(reinterpret_cast<DWORD**>(sv_cheats));
		original_get_bool = reinterpret_cast<SvCheatsGetBoolFn>(get_bool_manager.HookFunction<SvCheatsGetBoolFn>(13, HookedGetBool));

		fireevent.setup(g_csgo::GameEventManager);
		fireevent.hook_index(9, Hooked_FireEventClientSide);


		while (!(INIT::Window = FindWindowA("Valve001", nullptr)))
			Sleep(100);

		if (INIT::Window)
			INIT::OldWindow = (WNDPROC)SetWindowLongPtr(INIT::Window, GWL_WNDPROC, (LONG_PTR)Hooked_WndProc);

		DWORD DeviceStructureAddress = **(DWORD**)(UTILS::FindSignature("shaderapidx9.dll", "A1 ?? ?? ?? ?? 50 8B 08 FF 51 0C") + 1);

		if (DeviceStructureAddress) {

			directz.setup((DWORD**)DeviceStructureAddress);
			directz.hook_index(16, Hooked_EndScene_Reset);
			directz.hook_index(17, hooked_present);
			directz.hook_index(42, Hooked_EndScene);

		}
	}
	void EyeAnglesPitchHook(const SDK::CRecvProxyData *pData, void *pStruct, void *pOut)
	{
		*reinterpret_cast<float*>(pOut) = pData->m_Value.m_Float;

		auto entity = reinterpret_cast<SDK::CBaseEntity*>(pStruct);
		if (!entity)
			return;

	}
	void EyeAnglesYawHook(const SDK::CRecvProxyData *pData, void *pStruct, void *pOut)
	{
		*reinterpret_cast<float*>(pOut) = pData->m_Value.m_Float;

		auto entity = reinterpret_cast<SDK::CBaseEntity*>(pStruct);
		if (!entity)
			return;
	}
	void InitNetvarHooks()
	{
		UTILS::netvar_hook_manager.Hook("DT_CSPlayer", "m_angEyeAngles[0]", EyeAnglesPitchHook);
		UTILS::netvar_hook_manager.Hook("DT_CSPlayer", "m_angEyeAngles[1]", EyeAnglesYawHook);
	}
}

const char* key_binds[] = { "none", "mouse1", "mouse2", "mouse3", "mouse4", "mouse5", "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10", "f11", "f12" };

void MenuTabs::Tab1() {
	
	const char* hitboxes[] = { "head", "body aim", "hitscan" };

	ImGui::BeginChild("#aimbot_setting", ImVec2(320, 0), true); {

		ImGui::Checkbox("enable aimbot", &Settings::Options.aim_bool);

		ImGui::Text("aimbot mode");
		ImGui::Combo("", &Settings::Options.acc_type, hitboxes, ARRAYSIZE(hitboxes));

		ImGui::SliderInt("hit-chance", &Settings::Options.chance_val, 0, 100);
		ImGui::SliderInt("minimum damage", &Settings::Options.damage_val, 1, 100);
		ImGui::Checkbox(("more aimpoints"), &Settings::Options.multi_bool);
		if (Settings::Options.multi_bool)
		{
			ImGui::SliderFloat("head scale", &Settings::Options.point_val, 0, 1);
			ImGui::SliderFloat("body scale", &Settings::Options.body_val, 0, 1);
		}
		//ImGui::Combo("baim on key", &Settings::Options.baim_key, key_binds, sizeof(key_binds));
		ImGui::Checkbox(("auto stop"), &Settings::Options.stop_bool);
		ImGui::Checkbox(("lag compensation"), &Settings::Options.rage_lagcompensation);
		ImGui::Checkbox("fake walk", &Settings::Options.fakewalk);
		ImGui::SliderInt("fake walk speed", &Settings::Options.fakewalkspeed, 1, 14);
	} ImGui::EndChild();

	ImGui::SameLine();

	ImGui::BeginChild("#antiaims", ImVec2(320, 0), true); {

		const char* antiaimmode[] = { "standing", "moving", "jumping" };
		const char* aa_pitch[] = { "none", "emotion", "fake down", "fake up", "fake zero" };
		const char* aa_mode[] = { "none", "backwards", "sideways", "backjitter", "lowerbody", "legit troll", "rotational", "freestanding" };
		const char* aa_fake[] = { "none", "backjitter", "random", "local view", "opposite", "rotational" };

		ImGui::Checkbox(("enable hentai-aim"), &Settings::Options.aa_bool);
		ImGui::Combo(("mode"), &Settings::Options.aa_mode, antiaimmode, ARRAYSIZE(antiaimmode));

		switch (Settings::Options.aa_mode) {

			case 0: {

				ImGui::Combo(("pitch##st"), &Settings::Options.aa_pitch_type, aa_pitch, ARRAYSIZE(aa_pitch));
				ImGui::Combo(("real##st"), &Settings::Options.aa_real_type, aa_mode, ARRAYSIZE(aa_mode));
				//ImGui::Combo(("fake##st"), &Settings::Options.aa_fake_type, aa_fake, ARRAYSIZE(aa_fake));
				break;
			}

			case 1: {

				ImGui::Combo(("pitch##moving"), &Settings::Options.aa_pitch1_type, aa_pitch, ARRAYSIZE(aa_pitch));
				ImGui::Combo(("real##moving"), &Settings::Options.aa_real1_type, aa_mode, ARRAYSIZE(aa_mode));
				//ImGui::Combo(("fake##moving"), &Settings::Options.aa_fake1_type, aa_fake, ARRAYSIZE(aa_fake));
				break;
			}

			case 2: {

				ImGui::Combo(("pitch##jumping"), &Settings::Options.aa_pitch2_type, aa_pitch, ARRAYSIZE(aa_pitch));
				ImGui::Combo(("real##jumping"), &Settings::Options.aa_real2_type, aa_mode, ARRAYSIZE(aa_mode));
				//ImGui::Combo(("fake##jumping"), &Settings::Options.aa_fake2_type, aa_fake, ARRAYSIZE(aa_fake));
				break;
			}
		}
	} ImGui::EndChild();

	ImGui::SameLine();

	ImGui::BeginChild("#antiaimbuilder", ImVec2(320, 0), true); {

		ImGui::Checkbox(("hentai-aim arrows"), &Settings::Options.rifk_arrow);
		//ImGui::Combo(("Flip Key"), &Settings::Options.flip_int, key_binds, ARRAYSIZE(key_binds));
		switch (Settings::Options.aa_mode) {

			case 0: {

				ImGui::SliderFloat("add real", &Settings::Options.aa_realadditive_val, -180, 180);
				ImGui::SliderFloat("lowerbody delta", &Settings::Options.delta_val, -119.9, 119.9);
				break;
			}
			case 1: {

				ImGui::SliderFloat("add real", &Settings::Options.aa_realadditive1_val, -180, 180);
				ImGui::SliderFloat("lowerbody delta", &Settings::Options.delta1_val, -119.9, 119.9);
				break;
			}
			case 2: {

				ImGui::SliderFloat("add real", &Settings::Options.aa_realadditive2_val, -180, 180);
				ImGui::SliderFloat("lowerbody delta", &Settings::Options.delta2_val, -119.9, 119.9);
				break;
			}
		} ImGui::Spacing();
		switch (Settings::Options.aa_mode) {

			case 0: {

				ImGui::SliderFloat("standing angle �", &Settings::Options.spinangle, 0, 180);
				ImGui::SliderFloat("standing speed %", &Settings::Options.spinspeed, 0, 100);
				break;
			}
			case 1: {
				ImGui::SliderFloat("moving angle �", &Settings::Options.spinangle1, 0, 180);
				ImGui::SliderFloat("moving speed %", &Settings::Options.spinspeed1, 0, 100);
				break;
			}
			case 2: {
				ImGui::SliderFloat("jumping angle �", &Settings::Options.spinangle2, 0, 180);
				ImGui::SliderFloat("jumping speed %", &Settings::Options.spinspeed2, 0, 100);
				break;
			}
		}
	} ImGui::EndChild();
}

void MenuTabs::Tab2() {

	ImGui::BeginChild("#visuals", ImVec2(320, 0), true); {

		const char* weptype[] = { "type1", "type2" };

		ImGui::Checkbox(("enabled"), &Settings::Options.Visuals.Enabled);
		ImGui::Checkbox(("box"), &Settings::Options.Visuals.Visuals_BoxEnabled);
		ImGui::Combo(("box Type"), &Settings::Options.Visuals.Visuals_BoxType, weptype, ARRAYSIZE(weptype));
		ImGui::Checkbox(("esp team"), &Settings::Options.Visuals.Visuals_EspTeam);
		ImGui::Checkbox(("name"), &Settings::Options.Visuals.Visuals_Name);
		ImGui::Checkbox(("health"), &Settings::Options.Visuals.Visuals_HealthBar);
		ImGui::Combo(("health type"), &Settings::Options.Visuals.Visuals_HealthBarType, weptype, ARRAYSIZE(weptype));

		ImGui::Checkbox(("aimLines"), &Settings::Options.Visuals.Visuals_AimLines);
		ImGui::Checkbox(("weapons"), &Settings::Options.Visuals.Visuals_Weapons);
		ImGui::Combo(("weaponType"), &Settings::Options.Visuals.Visuals_WeaponsType, weptype, ARRAYSIZE(weptype));
		ImGui::Checkbox(("ammo"), &Settings::Options.Visuals.Visuals_AmmoESP);
		ImGui::Combo(("ammoType"), &Settings::Options.Visuals.Visuals_AmmoESPType, weptype, ARRAYSIZE(weptype));

		ImGui::Checkbox(("damage esp"), &Settings::Options.Visuals.Visuals_DamageESP);
		ImGui::SameLine(ImGui::GetWindowWidth() - 25);
		ImGui::ColorEdit4(("##DamageEspColor"), Settings::Options.Visuals.DamageESPColor, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::Checkbox(("fov arrows"), &Settings::Options.fov_bool);

	} ImGui::EndChild();

	ImGui::SameLine();

	ImGui::BeginChild("#visuals2", ImVec2(320, 0), true); {


		const char* chams_type[] = { "pearlescent", "material", "flat" };
		const char* chams_mode[] = { "none", "visible", "visible + invisible" };
		const char* glow_styles[] = { "regular", "pulsing", "outline" };
		const char* local_chams[] = { "flat", "material", "pearlescent" };

		ImGui::Combo(("enemy coloured models"), &Settings::Options.chams_type, chams_mode, ARRAYSIZE(chams_mode));
		ImGui::Combo(("team coloured models"), &Settings::Options.chamsteam, chams_mode, ARRAYSIZE(chams_mode));
		ImGui::Combo(("type chams"), &Settings::Options.chamstype, chams_type, ARRAYSIZE(chams_type));
		ImGui::Combo(("local chams"), &Settings::Options.localchams, local_chams, ARRAYSIZE(local_chams));

		ImGui::Checkbox(("fake chams"), &Settings::Options.fakechams);
		ImGui::Checkbox(("enemy glow enable"), &Settings::Options.glowenable);
		ImGui::Checkbox(("team glow enable"), &Settings::Options.glowteam);
		ImGui::Combo(("glow style"), &Settings::Options.glowstyle, glow_styles, ARRAYSIZE(glow_styles));

		ImGui::Checkbox(("local glow"), &Settings::Options.glowlocal);
		ImGui::Combo(("local glow style"), &Settings::Options.glowstylelocal, glow_styles, ARRAYSIZE(glow_styles));
	} ImGui::EndChild();

	ImGui::SameLine();

	ImGui::BeginChild("#visuals3", ImVec2(320, 0), true); {

		const char* crosshair_select[] = { "none", "static", "recoil" };
		const char* hitmarker[] = { "none", "gamesense", "bameware", "custom" };
		const char* spreadshit[] = { "off", "standart", "another", "rainbow" };

		ImGui::Checkbox("night mode", &Settings::Options.night_bool);
		ImGui::Checkbox(("bullet tracers"), &Settings::Options.beam_bool);
		ImGui::SliderFloat("bullet life", &Settings::Options.bulletlife, 0, 30);
		ImGui::SliderFloat("bullet size", &Settings::Options.bulletsize, 0, 20);

		ImGui::Checkbox(("thirdperson"), &Settings::Options.tp_bool);
		ImGui::Combo(("thirdperson key"), &Settings::Options.thirdperson_int, key_binds, ARRAYSIZE(key_binds));
		ImGui::SliderFloat(("thirdperson transparency"), &Settings::Options.transparency_amnt, 0, 1);
		ImGui::Combo(("crosshair"), &Settings::Options.xhair_type, crosshair_select, ARRAYSIZE(crosshair_select));
		ImGui::Combo(("spread circle"), &Settings::Options.spread_bool, spreadshit, ARRAYSIZE(spreadshit));

		ImGui::Checkbox(("remove smoke"), &Settings::Options.smoke_bool);
		ImGui::Checkbox(("remove flash"), &Settings::Options.no_flash);
		ImGui::Checkbox(("remove scope"), &Settings::Options.scope_bool);
		ImGui::Checkbox(("remove zoom"), &Settings::Options.removescoping);
		ImGui::Checkbox(("fix zoom sensitivity"), &Settings::Options.fixscopesens);

		ImGui::Checkbox(("enable postprocessing"), &Settings::Options.matpostprocessenable);
		//ImGui::Hotkey(("flashlight key"), &Settings::Options.flashlightkey);
		ImGui::SliderFloat("render fov", &Settings::Options.fov_val, 0, 179);
		ImGui::SliderFloat("viewmodel fov", &Settings::Options.viewfov_val, 0, 179);

		ImGui::Checkbox(("lby indicator"), &Settings::Options.lbyenable);
	} ImGui::EndChild();
}

void MenuTabs::Tab3() {

	ImGui::BeginChild("sa3rqewfadfwe", ImVec2(500, 0), true); {

		const char* fakelag_mode[] = { "factor", "adaptive" };

		ImGui::Checkbox(("auto bunnyhop"), &Settings::Options.bhop_bool);
		ImGui::Checkbox(("auto strafer"), &Settings::Options.strafe_bool);
		ImGui::Checkbox(("duck in air"), &Settings::Options.duck_bool);

		//ImGui::Hotkey(("circle strafer"), &Settings::Options.circlestrafekey, key_binds, ARRAYSIZE(key_binds));
		ImGui::Checkbox(("enable fakeLag"), &Settings::Options.lag_bool);
		ImGui::Combo(("fakelag type"), &Settings::Options.lag_type, fakelag_mode, ARRAYSIZE(fakelag_mode));
		ImGui::SliderFloat("standing lag", &Settings::Options.stand_lag, 1, 14);

		ImGui::SliderFloat("moving lag", &Settings::Options.move_lag, 1, 14);
		ImGui::SliderFloat("jumping lag", &Settings::Options.jump_lag, 1, 14);
	} ImGui::EndChild();
}

struct hud_weapons_t {
	std::int32_t* get_weapon_count() {
		return reinterpret_cast<std::int32_t*>(std::uintptr_t(this) + 0x80);
	}
};

template<class T>
static T* FindHudElement(const char* name)
{
	static auto pThis = *reinterpret_cast<DWORD**>(UTILS::FindSignature("client_panorama.dll", "B9 ? ? ? ? E8 ? ? ? ? 8B 5D 08") + 1);

	static auto find_hud_element = reinterpret_cast<DWORD(__thiscall*)(void*, const char*)>(UTILS::FindSignature("client_panorama.dll", "55 8B EC 53 8B 5D 08 56 57 8B F9 33 F6 39 77 28"));
	return (T*)find_hud_element(pThis, name);
}

void KnifeApplyCallbk() {

	static auto clear_hud_weapon_icon_fn =
		reinterpret_cast<std::int32_t(__thiscall*)(void*, std::int32_t)>(
			UTILS::FindSignature("client_panorama.dll", "55 8B EC 51 53 56 8B 75 08 8B D9 57 6B FE 2C 89 5D FC"));

	auto element = FindHudElement<std::uintptr_t*>("CCSGO_HudWeaponSelection");

	auto hud_weapons = reinterpret_cast<hud_weapons_t*>(std::uintptr_t(element) - 0xA0);
	if (hud_weapons == nullptr)
		return;

	if (!*hud_weapons->get_weapon_count())
		return;

	for (std::int32_t i = 0; i < *hud_weapons->get_weapon_count(); i++)
		i = clear_hud_weapon_icon_fn(hud_weapons, i);

	static SDK::ConVar* Meme = g_csgo::cvar->FindVar("cl_fullupdate");
	Meme->nFlags &= ~FCVAR_CHEAT;
	g_csgo::Engine->ClientCmd_Unrestricted("cl_fullupdate");

}

SDK::CBaseWeapon* xd(SDK::CBaseEntity* xz)
{
	if (!g_csgo::Engine->IsConnected())
		return nullptr;
	if (!xz->IsAlive())
		return nullptr;

	HANDLE weaponData = *(HANDLE*)((DWORD)xz + OFFSETS::m_hActiveWeapon);
	return (SDK::CBaseWeapon*)g_csgo::ClientEntityList->GetClientEntityFromHandle(weaponData);
}

short SafeWeaponID()
{
	SDK::CBaseEntity* local_player = g_csgo::ClientEntityList->GetClientEntity(g_csgo::Engine->GetLocalPlayer());
	if (!(local_player))
		return 0;

	SDK::CBaseWeapon* WeaponC = xd(local_player);

	if (!(WeaponC))
		return 0;

	return WeaponC->GetItemDefenitionIndex();
}

void MenuTabs::Tab4() {

	ImGui::BeginChild(("#skinchanger"), ImVec2(ImGui::GetWindowWidth() / 3 - 10, 0), true, true ? ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_ShowBorders | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoTitleBar : ImGuiWindowFlags_NoResize | ImGuiWindowFlags_ShowBorders); {

		ImGui::Text("DO NOT CLICK FORCE UPDATE WITHOUT \nENABLING SKIN CHANGER!", CColor(255, 255, 255));

		ImGui::Checkbox("skin changer", &Settings::Options.skinenabled);
		ImGui::Checkbox("glove changer", &Settings::Options.glovesenabled);
		//ImGui::Checkbox("model changer", &Settings::Options.CUSTOMMODEL);
		/*if (Settings::Options.CUSTOMMODEL)
		{
			ImGui::Combo(("ct"), &Settings::Options.customct, playermodels, ARRAYSIZE(playermodels));
			ImGui::Combo(("t"), &Settings::Options.customtt, playermodels, ARRAYSIZE(playermodels));
		}*/
		if (ImGui::Button(("Force update")))
			KnifeApplyCallbk();

	} ImGui::EndChild();

	ImGui::SameLine();

	ImGui::BeginChild(("#skinchanger_pages"), ImVec2(320, 0), true, true ? ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_ShowBorders | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoCollapse | ImGuiWindowFlags_NoScrollWithMouse | ImGuiWindowFlags_NoTitleBar : ImGuiWindowFlags_NoResize | ImGuiWindowFlags_ShowBorders); {

		static bool Main = true;
		static bool Colors = false;

		static int page = 0;

		ImGuiStyle& style = ImGui::GetStyle();


		style.ItemSpacing = ImVec2(1, 1);
		ImGui::PushStyleColor(ImGuiCol_Button, ImVec4(0.1f, 0.1f, 0.1f, 1.f));
		ImGui::PushStyleColor(ImGuiCol_ButtonHovered, ImVec4(0.2f, 0.2f, 0.2f, 0.8f));
		ImGui::PushStyleColor(ImGuiCol_ButtonActive, ImVec4(0.2f, 0.2f, 0.2f, 1.f));

		const char* meme = "page : error";

		switch (page) {

			case 0: meme = "page : 1";  break;
			case 1: meme = "page : 2";  break;
			case 2: meme = "page : 3";  break;
			case 3: meme = "page : 4";  break;
			case 4: meme = "page : 5";  break;
			default: break;
		}

		ImGui::Text(meme); ImGui::SameLine(); ImGui::Text(("                  ")); ImGui::SameLine();
		if (ImGui::Button(("-"), ImVec2(22, 22))) {

			if (page != 0)
				page--;
		};

		ImGui::SameLine();

		if (ImGui::Button(("+"), ImVec2(22, 22))) {

			if (page != 3)
				page++;
		};

		ImGui::Text(("        "));

		ImGui::PopStyleColor(); ImGui::PopStyleColor(); ImGui::PopStyleColor();

		style.ItemSpacing = ImVec2(8, 4);

		switch (page) {

		case 0: {

			ImGui::PushItemWidth(150.0f);
			ImGui::Combo(("knife model"), &Settings::Options.Knife, KnifeModel, ARRAYSIZE(KnifeModel));
			ImGui::PushItemWidth(150.0f);
			ImGui::Combo(("knife skin"), &Settings::Options.KnifeSkin, knifeskins, ARRAYSIZE(knifeskins));
			ImGui::PushItemWidth(150.0f);
			ImGui::Combo(("glove model"), &Settings::Options.gloves, GloveModel, ARRAYSIZE(GloveModel));
			ImGui::PushItemWidth(150.0f);

			switch (Settings::Options.gloves) {

				case 0: {
					const char* glovesskins[] = { "" };
					ImGui::Combo(("glove skin"), &Settings::Options.skingloves, glovesskins, ARRAYSIZE(glovesskins));
					break;
				}
				case 1: {
					const char* glovesskins[] = { "charred", "snakebite", "bronzed", "guerilla" };
					ImGui::Combo(("glove skin"), &Settings::Options.skingloves, glovesskins, ARRAYSIZE(glovesskins));
					break;
				}
				case 2: {
					const char* glovesskins[] = { "hedge maze", "andoras box", "superconductor", "arid", "omega", "amphibious", "bronze morph" };
					ImGui::Combo(("glove skin"), &Settings::Options.skingloves, glovesskins, ARRAYSIZE(glovesskins));
					break;
				}
				case 3: {
					const char* glovesskins[] = { "lunar weave", "convoy", "crimson weave", "diamondback", "overtake", "racing green", "king snake", "imperial plaid" };
					ImGui::Combo(("glove skin"), &Settings::Options.skingloves, glovesskins, ARRAYSIZE(glovesskins));
					break;
				}
				case 4: {
					const char* glovesskins[] = { "leather", "spruce ddpat", "slaughter", "cobalt skulls", "overprint", "duct tape", "arboreal" };
					ImGui::Combo(("glove skin"), &Settings::Options.skingloves, glovesskins, ARRAYSIZE(glovesskins));
					break;
				}
				case 5: {
					const char* glovesskins[] = { "eclipse", "spearmint", "boom", "cool mint", "turtle", "transport", "polygon", "pow" };
					ImGui::Combo(("glove skin"), &Settings::Options.skingloves, glovesskins, ARRAYSIZE(glovesskins));
					break;
				}
				case 6: {
					const char* glovesskins[] = { "forest ddpat", "crimson kimono", "emerald web", "foundation", "crimson web", "buckshot", "fade", "mogul" };
					ImGui::Combo(("glove skin"), &Settings::Options.skingloves, glovesskins, ARRAYSIZE(glovesskins));
					break;
				}
				case 7: {
					const char* glovesskins[] = { "emerald", "mangrove", "rattler", "case hardened" };
					ImGui::Combo(("glove skin"), &Settings::Options.skingloves, glovesskins, ARRAYSIZE(glovesskins));
					break;
				}
			}

		} break;
		case 1:
		{
			ImGui::Combo(("AK-47"), &Settings::Options.AK47Skin, ak47, ARRAYSIZE(ak47));
			ImGui::Combo(("M4A1-S"), &Settings::Options.M4A1SSkin, m4a1s, ARRAYSIZE(m4a1s));
			ImGui::Combo(("M4A4"), &Settings::Options.M4A4Skin, m4a4, ARRAYSIZE(m4a4));
			ImGui::Combo(("Galil AR"), &Settings::Options.GalilSkin, galil, ARRAYSIZE(galil));
			ImGui::Combo(("AUG"), &Settings::Options.AUGSkin, aug, ARRAYSIZE(aug));
			ImGui::Combo(("FAMAS"), &Settings::Options.FAMASSkin, famas, ARRAYSIZE(famas));
			ImGui::Combo(("SG-553"), &Settings::Options.Sg553Skin, sg553, ARRAYSIZE(sg553));
			ImGui::Combo(("UMP45"), &Settings::Options.UMP45Skin, ump45, ARRAYSIZE(ump45));
			ImGui::Combo(("MAC-10"), &Settings::Options.Mac10Skin, mac10, ARRAYSIZE(mac10));
			ImGui::Combo(("PP-Bizon"), &Settings::Options.BizonSkin, bizon, ARRAYSIZE(bizon));
			ImGui::Combo(("TEC-9"), &Settings::Options.tec9Skin, tec9, ARRAYSIZE(tec9));
			ImGui::Combo(("P2000"), &Settings::Options.P2000Skin, p2000, ARRAYSIZE(p2000));
			ImGui::Combo(("P250"), &Settings::Options.P250Skin, p250, ARRAYSIZE(p250));
			ImGui::Combo(("Dual-Barettas"), &Settings::Options.DualSkin, dual, ARRAYSIZE(dual));
			ImGui::Combo(("CZ75-Auto"), &Settings::Options.Cz75Skin, cz75, ARRAYSIZE(cz75));
			ImGui::Combo(("Nova"), &Settings::Options.NovaSkin, nova, ARRAYSIZE(nova));


		} break;
		case 2:
		{
			ImGui::Combo(("XM1014"), &Settings::Options.XmSkin, xm, ARRAYSIZE(xm));
			ImGui::Combo(("AWP"), &Settings::Options.AWPSkin, awp, ARRAYSIZE(awp));
			ImGui::Combo(("SSG08"), &Settings::Options.SSG08Skin, ssg08, ARRAYSIZE(ssg08));
			ImGui::Combo(("SCAR20"), &Settings::Options.SCAR20Skin, scar20, ARRAYSIZE(scar20));
			ImGui::Combo(("G3SG1"), &Settings::Options.G3sg1Skin, g3sg1, ARRAYSIZE(g3sg1));
			ImGui::Combo(("MP9"), &Settings::Options.Mp9Skin, mp9, ARRAYSIZE(mp9));
			ImGui::Combo(("Glock-18"), &Settings::Options.GlockSkin, glock, ARRAYSIZE(glock));
			ImGui::Combo(("USP-S"), &Settings::Options.USPSkin, usp, ARRAYSIZE(usp));
			ImGui::Combo(("Desert Eagle"), &Settings::Options.DeagleSkin, deagle, ARRAYSIZE(deagle));
			ImGui::Combo(("Five-Seven"), &Settings::Options.FiveSkin, five, ARRAYSIZE(five));
			ImGui::Combo(("Revolver"), &Settings::Options.RevolverSkin, revolver, ARRAYSIZE(revolver));
			ImGui::Combo(("Negev"), &Settings::Options.NegevSkin, negev, ARRAYSIZE(negev));
			ImGui::Combo(("M249"), &Settings::Options.M249Skin, m249, ARRAYSIZE(m249));
			ImGui::Combo(("SAWED-Off"), &Settings::Options.SawedSkin, sawed, ARRAYSIZE(sawed));
			ImGui::Combo(("Mag-7"), &Settings::Options.MagSkin, mag, ARRAYSIZE(mag));
		} break;
		default:break;
		}
	} ImGui::EndChild();
}

void MenuTabs::Tab5() {

	ImGui::BeginChild("#configs", ImVec2(400, 0), true); {

			ImGui::InputText("##configname", ConfigNamexd, 64);
			static int sel;
			std::string config;

			std::vector<std::string> configs = Settings::Options.GetConfigs();

			if (configs.size() > 0) {

				ImGui::ComboBoxArrayxd("configs", &sel, configs);
				ImGui::Spacing();
				ImGui::Separator();
				ImGui::Spacing();
				ImGui::PushItemWidth(220.f);
				config = configs[Settings::Options.config_sel];
			} Settings::Options.config_sel = sel;

			if (configs.size() > 0) {

				if (ImGui::Button("load", ImVec2(50, 20))) {

					Settings::Options.Load(config);
					g_csgo::cvar->ConsoleColorPrintf(CColor(255, 20, 255), "[Wanheda] ");
					g_csgo::cvar->ConsoleColorPrintf(CColor(255, 255, 255), "loaded config: %s\n", config.c_str());
				}
			} ImGui::SameLine();

			if (configs.size() >= 1) {

				if (ImGui::Button("save", ImVec2(50, 20))) {

					Settings::Options.Save(config);
					g_csgo::cvar->ConsoleColorPrintf(CColor(255, 20, 255), "[Wanheda] ");
					g_csgo::cvar->ConsoleColorPrintf(CColor(255, 255, 255), "saved config: %s\n", config.c_str());
				}
			} ImGui::SameLine();

			if (ImGui::Button("create", ImVec2(50, 20))) {

				std::string ConfigFileName = ConfigNamexd;

				if (ConfigFileName.size() < 1)
					ConfigFileName = "settings";

				Settings::Options.CreateConfig(ConfigFileName);
				g_csgo::cvar->ConsoleColorPrintf(CColor(255, 20, 255), "[Wanheda] ");
				g_csgo::cvar->ConsoleColorPrintf(CColor(255, 255, 255), "created config: %s\n", ConfigFileName.c_str());
			} ImGui::SameLine();

			if (config.size() >= 1) {

				if (ImGui::Button("delete", ImVec2(50, 20))) {

					Settings::Options.Remove(config);
					g_csgo::cvar->ConsoleColorPrintf(CColor(255, 20, 255), "[Wanheda] ");
					g_csgo::cvar->ConsoleColorPrintf(CColor(255, 255, 255), "removed config: %s\n", config.c_str());
				}
			}
	} ImGui::EndChild();

	ImGui::SameLine();

	ImGui::BeginChild("#colors", ImVec2(500, 0), true); {

		ImGui::Text(("box colors"));
		ImGui::ColorEdit4(("CT Visible##box"), Settings::Options.Visuals.BoxColorPickCTVIS, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::ColorEdit4(("CT invisible##box"), Settings::Options.Visuals.BoxColorPickCTINVIS, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::ColorEdit4(("T Visible##box"), Settings::Options.Visuals.BoxColorPickTVIS, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::ColorEdit4(("T invisible##box"), Settings::Options.Visuals.BoxColorPickTINVIS, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);


		ImGui::Text(("chams"));
		ImGui::ColorEdit4(("Enemy visible##chams"), Settings::Options.vmodel_col, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::ColorEdit4(("Enemy invisible##chams"), Settings::Options.imodel_col, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::ColorEdit4(("Team visible##chams"), Settings::Options.teamvis_color, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::ColorEdit4(("Team invisible##chams"), Settings::Options.teaminvis_color, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::ColorEdit4(("Local##chams"), Settings::Options.localchams_col, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);


		ImGui::Text(("glow"));
		ImGui::ColorEdit4(("Enemy##glow"), Settings::Options.glow_col, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::ColorEdit4(("Team##glow"), Settings::Options.teamglow_color, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::ColorEdit4(("Local##glow"), Settings::Options.glowlocal_col, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::Text(("bullet tracers"));

		ImGui::ColorEdit4(("Local##tracer"), Settings::Options.bulletlocal_col, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::ColorEdit4(("Enemy##tracer"), Settings::Options.bulletenemy_col, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::ColorEdit4(("Team##tracer"), Settings::Options.bulletteam_col, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);

		ImGui::Text(("other"));
		//ImGui::ColorEdit4(("grenade prediction##xd"), Settings::Options.grenadepredline_col, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		ImGui::ColorEdit4(("spread circle##xd"), Settings::Options.spreadcirclecol, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
		//ImGui::ColorEdit4(("fake chams##xd"), Settings::Options.fakechamscol, ImGuiColorEditFlags_NoInputs | ImGuiColorEditFlags_NoSidePreview | ImGuiColorEditFlags_AlphaBar);
	} ImGui::EndChild();
}